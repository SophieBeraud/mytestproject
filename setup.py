from setuptools import setup
import setuptools

setup(
    name='ExamVendredi',
    version='0.0.1',
    author="afa",
    description="TestSimpleCalculator is a simple package \
    in order to make some test on packaging principles in Python",
    license='GNU GPLv3',
    python_requires ='>=2.4',
    packages=['package_test', 'test'],
)
